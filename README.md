# VDAT DevOps Test1

The awesomeProject demo for VDAT team's DevOps test.

Build project using Docker compose

`docker-compose up`

Check your localhost at [8080](localhost:8080)
