package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
)

var (
	db   *sql.DB
	err  error
	port = os.Getenv("PORT")
)

func helloHandler(w http.ResponseWriter, req *http.Request) {
	rows, err := db.Query("SELECT 1+1")
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		var dest string
		if err := rows.Scan(&dest); err != nil {
			panic(err)
		} else if dest != "2" {
			_, _ = fmt.Fprintf(w, "I don't feel so good :(\n")
			return
		}
		_, _ = fmt.Fprintf(w, "Hello there!\n")
	}
}

func main() {
	db, err = sql.Open("postgres",
		fmt.Sprintf("postgres://postgres:%s@db:5432/postgres?sslmode=disable",
			os.Getenv("POSTGRES_PASSWORD")))
	if err != nil {
		log.Fatal(err)
	}
	if port == "" {
		port = "3000"
	}
	http.HandleFunc("/", helloHandler)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), nil))
}
