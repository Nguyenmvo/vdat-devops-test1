FROM golang:1.14
WORKDIR /go/src/awesomeProject
COPY go.* ./
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /go/bin
COPY --from=0 /go/src/awesomeProject/app .
CMD ["./app"]